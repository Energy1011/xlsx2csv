FROM python:3.7

WORKDIR /script

RUN apt update

COPY xlsx2csv.py .

COPY requirements.txt .

RUN mkdir /script/xlsx

RUN mkdir /script/output

COPY xlsx/demo.xlsx ./xlsx

RUN pip3 install -r requirements.txt

CMD ["python3", "/script/xlsx2csv.py"]