#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Author: energy1011

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import logging as log
import pandas as pd
import os
from pathlib import Path

APP="xlsx2csv"

class Script():
    input_file=None
    output_path=None
    sheets_to_use=None
    prefix_csv_filename=None

    def __init__(self, args):
        self.input_file = args.input_file 
        self.output_path= args.output_path
        self.sheets_to_use = args.sheets_to_use
        self.prefix_csv_filename = args.prefix_csv_filename
        self.validate_params()
        pass

    def create_output_path(self, path):
        Path(path).mkdir(parents=True, exist_ok=True)
        pass

    def pathExists(self, path):
        return os.path.exists(path)

    def validate_params(self):
        #Check for sheets indexes passing by args
        if not self.sheets_to_use:
            log.debug("Processing all pages because without -s, --sheets option")
        else:
            sheets = ''.join(str(self.sheets_to_use))
            log.debug("Index of sheets to process: "+sheets)

        # Check for output path arg
        if self.output_path is not None:
            # Verify path ends with os separator '/' or '\'
            if self.output_path[-1] != os.sep:
                self.output_path = self.output_path + os.sep
            # create intermediate non created dirs
            self.ensure_dir(self.output_path)
            if not os.path.exists(self.output_path):
                log.critical("Error ouput path '%s'", self.output_path)
                exit(1)
        pass

    def ensure_dir(self, file_path):
        directory = os.path.dirname(file_path)
        if not os.path.exists(directory):
            try:
                os.makedirs(directory)
                pass
            except PermissionError as error:
                log.critical("Check for correct path '%s' or permissions", file_path)
                exit(1)
                pass

    def convert_sheet_to_csv(self, sheet_name):
        sheet = pd.read_excel(self.input_file, sheet_name=sheet_name)
        
        # Set csv filename
        if self.prefix_csv_filename:
            filename = "{}{}.csv".format(self.prefix_csv_filename, sheet_name)
        else:
            filename = "{}.csv".format(sheet_name)

        if self.output_path is not None:
            filename = self.output_path + filename

        #convert
        sheet.to_csv(filename, index=False, encoding='utf-8')
        log.debug("File converted and written '%s'", filename)

    def convert_iterating_sheet_indexes(self):
        for s_index in self.sheets_to_use:
            try:
                sheet_name = list(self.sheet_names)[s_index]
                log.debug("Converting sheet with index '%s' named: '%s'", str(s_index), sheet_name)
                self.convert_sheet_to_csv(sheet_name)
                pass
            except IndexError as error:
                log.critical("%i is a -s option number out of range, check the number of sheets in the file '%s'", s_index, self.input_file) 
                exit(1)
                pass
        pass

    def convert_iterating_sheet_names(self):
        for sheet_name in self.sheet_names:
                log.debug("Converting sheet named: '%s'", sheet_name)
                self.convert_sheet_to_csv(sheet_name)
                pass
        pass
        
    def read_file(self):
        log.debug("Reading input file: "+self.input_file)
        self.all_sheets = pd.read_excel(self.input_file, sheet_name=None)
        self.sheet_names = self.all_sheets.keys()
        log.debug("Sheets in file: ")
        log.debug(self.sheet_names)
        if self.sheets_to_use :
            self.convert_iterating_sheet_indexes()
        else:
            self.convert_iterating_sheet_names()

    def convert(self):
        self.read_file()
        pass

if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(prog=APP, description='xlsx to csv')
    parser.add_argument('-i', '--input-file', dest='input_file', default=None, help='Input file path', required=True)
    parser.add_argument('-o', '--out-path', dest='output_path', default=None, help='Output file path')
    parser.add_argument('-l', '--loglevel', dest='loglevel', default=log.INFO, help='Logging level: INFO, DEBUG, WARNING, CRITICAL, ERROR')
    parser.add_argument('-s', '--sheets', dest='sheets_to_use', metavar='N', type=int, nargs='+', help='Integer list separated by space, if you want for example process first, second and the fourth sheet use like this: -s 0 1 3 (notice index starts counting by zero)')
    parser.add_argument('-p', '--prefix-csv-filename', dest='prefix_csv_filename', default=None, help='Prefix csv filename by a date or any string example: -p 17-12-2020')

    # get args
    args = parser.parse_args()

    # set log 
    log.basicConfig(format=APP+' %(levelname)s: %(message)s', level=args.loglevel) 

    s = Script(args)
    s.convert()
    exit(0)