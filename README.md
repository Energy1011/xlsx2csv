# XLSX2CSV
This script is used to convert sheets from xlsx files to csv files

# Requirements
pip3 and python3 is needed in your system or docker for docker running mode.

# Install dependencies

Clone and run in repository directory this command
```
git clone git@gitlab.com:Energy1011/xlsx2csv.git
cd xlsx2csv
```

Install xlsxcsv dependencies
```bash
pip3 install -r requirements.txt
```

# Usage and options
To see what options are available to you:
```bash
python3 xlsx2csv -h
```

Converting a xlsx file example
```
python3 xlsx2csv.py -i demo.xlsx -s 0 1 2 -o /path/to/my/csv/
```
- -i (required) option for input xlsx file
- -s (optional) option for sheets to convert into separated csv files (0 , 1, 2 ) corresponds to sheet1, sheet2 and sheet3
- -o (optional) option for output path for csv files, default is current directory
- -l (optional) option for logging level, example: -l DEBUG
- -p (optional) Prefix csv filename by a date or any string example: -p 17-12-2020

# Running using docker container
First clone repository and build a taged image 'xlsx2csv'
```
sudo docker build . -t xlsx2csv
```

To run demo or your own xlsx file run something like this following command 
```bash
sudo docker run --rm -v $(pwd):/script/output --name xlsx2csv energy10/xlsx2csv:latest python3 xlsx2csv.py -i xlsx/demo.xlsx -o /script/output -l DEBUG && ls
```
--rm option is removing container after finished :)

NOTICE: Important thing using -v (volume of current working directory) to internal /script WORKDIR inside container

Enjoy.

